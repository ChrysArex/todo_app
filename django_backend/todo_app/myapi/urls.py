from django.urls import path
from . import views

urlpatterns = [
    path('all-todo/<int:id>/<str:status>/', views.all_todo, name='all_todo'),
    path('update_title-todo/<int:id>/<str:title>/', views.update_title_todo, name='update_title_todo'),
    path('update_status-todo/<int:id>/<str:status>/', views.update_status_todo, name='update_status_todo'),
    path('create-todo/<int:users_id>/<str:title>/', views.create_todo, name='create_todo'),
    path('delete-todo/<int:id>/', views.delete_todo, name='delete_todo'),

    path('all-user/', views.all_user, name='all_user'),
    path('update_fname-user/<int:id>/<str:first_name>/', views.update_fname_user, name='update_fname_user'),
    path('update_lname-user/<int:id>/<str:last_name>/', views.update_lname_user, name='update_lname_user'),
    path('create-user/<str:first_name>/<str:last_name>/', views.create_user, name='create_user'),
    path('delete-user/<int:id>/', views.delete_user, name='delete_user'),
]
