from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from . import models
"""CRUD functions for todos"""
@api_view(['POST'])
def create_todo(request, users_id, title):
    todo = models.Task(users_id=users_id, title=title)
    try:
        todo.save()
        return Response("todo created succesfully")
    except:
        return Response("todo to create the user")

@api_view(['GET'])
def all_todo(request, id, status):
    todos = list(models.Task.objects.filter(users_id=id, status=status).values())
    return Response(todos)

@api_view(['PUT'])
def update_title_todo(request, id, title):
    todo = models.Task.objects.get(id=id)
    todo.title = title
    try:
        todo.save()
        return Response("update succeeded")
    except:
        return Response("update failed")

@api_view(['PUT'])
def update_status_todo(request, id, status):
    todo = models.Task.objects.get(id=id)
    todo.status = status
    try:
        todo.save()
        return Response("update succeeded")
    except:
        return Response("update failed")

@api_view(['DELETE'])
def delete_todo(request, id):
    todo = models.Task.objects.get(id=id)
    try:
        todo.delete()
        return Response("todo deleted successfuly")
    except:
        return Response("failed to delete the todo")

"""CRUD functions for users"""
@api_view(['POST'])
def create_user(request, first_name, last_name):
    user = models.User(first_name=first_name, last_name=last_name)
    try:
        user.save()
        return Response("user created succesfully")
    except:
        return Response("failed to create the user")

@api_view(['GET'])
def all_user(request):
    try:
        users = list(models.User.objects.all().values())
        return Response(users)
    except:
        return Response("Failed to retrieve all users")

@api_view(['PUT'])
def update_fname_user(request, id, first_name):
    user = models.User.objects.get(id=id)
    user.first_name = first_name
    try:
        user.save()
        return Response("update succeeded")
    except:
        return Response("update failed")

@api_view(['PUT'])
def update_lname_user(request, id, last_name):
    user = models.User.objects.get(id=id)
    user.last_name = last_name
    try:
        user.save()
        return Response("update succeeded")
    except:
        return Response("update failed")

@api_view(['DELETE'])
def delete_user(request, id):
    user = models.User.objects.get(id=id)
    try:
        user.delete()
        return Response("user deleted successfuly")
    except:
        return Response("failed to delete the user")

