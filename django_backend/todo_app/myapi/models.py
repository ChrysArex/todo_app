from django.db import models

# Create your models here.
class User(models.Model):
    """define a user attributes"""
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

class Task(models.Model):
    """define a user attributes"""
    title = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="todo")
    users = models.ForeignKey(User, on_delete=models.CASCADE)


