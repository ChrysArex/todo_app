<h2>Ce dossier represente le backend de la todo app.</h2>

<h3>Installation:</h3>
<h5>Prérequis:</h5>

Vous devez avoir une base de donner postgresql possedant un utilisateur `postgres` dont le mot de passe est `todo` et une base de donnée`todo_db` mais vous pouver egalement adapter les parametres de connexion dans le fichier `django_backend/todo_app/settings.py`

**Déplacer vous vers le dossier django_backend et créer un environnement virtuelle avec la commande `python -m venv <nom_env>`**<br/>
**Activer l'environement crée:** <br/>
###### Sous Windows<br/>
`.\<nom_env>\Scripts\Activate` <br/>
###### Sous Linux<br/>
`source <nom_env>/bin/activate`<br/>

**installer les dependances:**

  `pip3 install django`

  `pip3 install djangorestframework django-cors-headers`

<h3>Tester l'api:</h3>

**Lancer le serveur de test avec la commande `python manage.py runserver`**

**Effectuer les requetes souhaiter vers le socket `127.0.0.1:8000` avec `curl` ou `postman`**

    /api/all-todo/<id>/<status>/: renvois la list de todos ayant le status <status> de l'utilisateur dont l'id est <id>
    /api/update_title-todo/<id>/<title>/: donne la valeur <title> au todo d'id <id>
    /api/update_status-todo/<id>/<status>/: donne la valeur <status> au todo d'id <id>
    /api/create-todo/<users_id>/<title>/: crée un todo intitulée <title> pour l'utilisateur d'id <users_id>
    /api/delete-todo/<id>/: supprime le todo d'id <id>

    /api/all-user/: renvoie la liste de tous les utilisateurs
    /api/update_fname-user/<id>/<first_name>/: donne la valeur <first_name> a l'utilisateur d'id <id>
    /api/update_lname-user/<id>/<last_name>/: donne la valeur <last_name> a l'utilisateur d'id <id>
    /api/create-user/<first_name>/<last_name>/: crée l'utilisateur de prenom <first_name> et de nom <last_name>
    /api/delete-user/<id>/: supprime l'utilisateur d'id <id>
