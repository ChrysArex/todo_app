import { useParams, NavLink } from 'react-router-dom'
import Header from '../Header'
import FormN from '../formulaire/FormN'
import Todo from './Todo'
import { useState, useEffect } from 'react'
import axios from 'axios'   

export default function UserInterface() {

    const {slug} = useParams()
    const [nom, prenom, id] =  slug.split(':')

    const [todo, setTodo] = useState([])
    const [doing, setDoing] = useState([])
    const [done, setDone] = useState([])
    const getTodo = async () => {
        try {
            const restodo = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/todo/`)
            const resdoing = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/doing/`);
            const resdone = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/done/`);

            setTodo(restodo.data);
            setDoing(resdoing.data);
            setDone(resdone.data);
        } catch (error) {
            if (error.code === 'ECONNABORTED') {
                console.log('La requête a expiré en raison d\'un délai d\'attente dépassé.');
            } else {
                console.log('Erreur lors de la requête:', error.message);
            }
        }
    };
    
    const getDoing = async () => {
        try {
            const restodo = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/todo/`)
            const resdoing = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/doing/`);
            const resdone = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/done/`);

            setTodo(restodo.data);
            setDoing(resdoing.data);
            setDone(resdone.data);

        } catch (error) {
            if (error.code === 'ECONNABORTED') {
                console.log('La requête a expiré en raison d\'un délai d\'attente dépassé.');
            } else {
                console.log('Erreur lors de la requête:', error.message);
            }
        }
    };
    
    const getDone = async () => {
        try {
            const restodo = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/todo/`)
            const resdoing = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/doing/`);
            const resdone = await axios.get(`http://127.0.0.1:8000/api/all-todo/${id}/done/`);

            setTodo(restodo.data);
            setDoing(resdoing.data);
            setDone(resdone.data);

        } catch (error) {
            if (error.code === 'ECONNABORTED') {
                console.log('La requête a expiré en raison d\'un délai d\'attente dépassé.');
            } else {
                console.log('Erreur lors de la requête:', error.message);
            }
        }
    };
   

    useEffect(() => {
        getTodo();
        getDoing();
        getDone();
      }, []);

    
    return (
        <>
            <div className='text-left mb-5'>
                    <NavLink className = "hover:bg-cyan-800 rounded-lg span p-2" to={'/'}>
                        <i className="fa-solid fa-arrow-left text-sm"></i>
                        <span className='text-sm pl-2'>Retour</span>
                    </NavLink>   
            </div>
            <Header 
                head={nom + " " + prenom}
                initials={nom.substring(0,1) + prenom.substring(0,1)}
            />
            <div className='flex flex-col lg:flex-row justify-between w-11/12 ml-5 mt-5'>
                <div className='min-w-52'>
                    <h1 className='bg-red-800 p-2 rounded-lg mb-2'>À Faire</h1>
                    {
                        todo.map(
                            (data) => {
                                return (
                                    <Todo data={data} key={data.id} getElt = {getTodo}/>
                                )
                            }
                        )
                    }
                    <FormN placeHolder = {"+ Nouveau"} getUser = {getTodo} user_id = {id}/>
                </div>
                <div className='min-w-52'>
                    <h1 className='bg-yellow-800 p-2 rounded-lg mb-2'>En cours</h1>
                    {
                        doing.map(
                            (data) => {
                                return (
                                    <Todo data={data} key={data.id} getElt = {getDoing}/>
                                )
                            }
                        )
                    }
                    
                </div>
                <div className='min-w-52'>
                    <h1 className='bg-green-800 p-2 rounded-lg mb-2'>Terminé</h1>
                    {
                        done.map(
                            (data) => {
                                return (
                                    <Todo data={data} key={data.id} getElt = {getDone}/>
                                )
                            }
                        )
                    }
                    
                </div>
            </div>
            
        </>
    )
}