import Finput from "../formulaire/Finput"
import Action from "../admin/Action"
import axios from "axios"
import { useState } from "react"


export default function Todo({data, getElt}) {
    const style = "w-72 bg-neutral-900 hover:bg-zinc-800 p-2 rounded-lg mb-3 mr-2 text-xs inline-block text-left todo"
    const id = data.id
    const [display, setDisplay] = useState(false)
    const [btnClicked, setBtnclicked] = useState(null)

    const handleClick = (elt) => {
        setBtnclicked(elt)
    }
    const deleteNote = async () => {
        axios.delete(`http://127.0.0.1:8000/api/delete-todo/${id}/`, {timeout: 5000}).then(res => {
            getElt()
        }).catch(error => {
            console.log(error);
        });
    }
    const updateNote = async (status) =>{
        axios.put(`http://127.0.0.1:8000/api/update_status-todo/${id}/${status}/`, {timeout: 5000}).then(res => {
            getElt()
        }).catch(error => {
            console.log(error);
        });
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        updateNote(btnClicked)
    }
    return (
        <>
            <form onSubmit={handleSubmit}>
                <div className={style}>
                    {data.title}
                </div> 
                <Action 
                    title={"Supprimer"}
                    icon={<i className="fa-solid fa-trash"></i>}
                    Style = {"text-base hover:bg-red-600 px-2 py-1 rounded-lg mr-2 span cursor-pointer"}
                    onClick = {deleteNote}
                />
                <span 
                    className="text-base hover:bg-cyan-800 px-2 py-1 rounded-lg  span cursor-pointer"
                    onClick={() => setDisplay(!display)}
                    title="Modifier l'état"
                >
                    <i className="fa-solid fa-ellipsis-vertical"></i>
                </span>
                {
                    display && 
                    <div className="flex flex-col items-center text-sm">
                        <h4 className="p-2 bg-zinc-800 rounded-lg mb-1">Modifier l'état </h4>
                        <button 
                            className ="rounded-lg hover:bg-red-600 text-xs p-2 w-28 mb-1" 
                            type="submit"
                            onClick={()=> handleClick("todo")}>
                            À faire
                        </button>
                        <button 
                            className ="rounded-lg hover:bg-yellow-800 text-xs p-2 w-28 mb-1 " 
                            type="submit"
                            onClick={() => handleClick("doing")}>
                            En cours
                        </button>
                        <button 
                            className ="rounded-lg hover:bg-green-800 text-xs p-2 w-28 mb-1" 
                            type="submit"
                            onClick={() => handleClick("done")}>
                            Terminé
                        </button>
                    </div>
                }
            </form>
        </>
    )
}