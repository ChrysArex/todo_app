import Finput from "./Finput"
import Botton from "./Botton"
import axios from "axios"
import { useState } from "react"
export default function FormN({placeHolder, getUser, user_id}) {
    const style = "flex flex-col justify-start items-end "
    const styleInput = "w-full h-8 hover:bg-zinc-800 p-2 rounded-lg mb-3 text-xs"
    const [tache, setTache] = useState("")
    const handleChange = (e) =>{
        setTache(e.target.value)
    }
    const addNote = async () => {
        try {
            await axios.post(`http://127.0.0.1:8000/api/create-todo/${user_id}/${tache}/`, {}, {timeout: 5000});
            getUser();
        } catch (error) {
            console.log('Erreur lors de l\'ajout des données: ', error);
        }
    }
    const handleSubmit = (e) =>{
        e.preventDefault();
        if (tache == "")
        {
            alert("Cette tâche est vide remplissez là")
            return
        }
        addNote()
        setTache('')
    }
   
    return (
        <form action="" className={style} onSubmit={handleSubmit}>
            <Finput type = {"test"} placeholder={placeHolder} value={tache} onChange={handleChange} style={styleInput}/>
            <button type="submit" className="w-24 h-8 bg-cyan-800 rounded-lg text-sm hover:bg-cyan-600">+ Ajouter</button>
        </form>
    )
}