import Finput from "./Finput"
import Botton from "./Botton"
import axios from "axios"
import { useState } from "react"
export default function Form({placeHolder, getUser}) {
    const style = "flex flex-col justify-start items-end ml-5 lg:ml-0 "
    const styleInput = "h-10 w-full hover:bg-zinc-800 p-2 text-xs rounded-lg mb-3 lg:w-72 lg:text-base"
    const [fullname, setFullname] = useState("")
    const regex = /^[a-zA-Z0-9]*\s[a-zA-Z0-9]*$/;
    
    const handleChange = (e) =>{
        setFullname(e.target.value)
    }
    const handleSubmit = (e) =>{
        e.preventDefault();
        if (!fullname.match(regex))
        {
            alert("Le nom complet ne doit pas contenir de caractère spéciaux et doit contenir un seul espace. Ex: ADOUMASSE Ortniel")
            setFullname('')
            return
        }
        addUser()
        setFullname('')
    }
    const addUser = async () => {
        const [first_name, last_name] = fullname.split(' ');
        try {
            await axios.post(`http://127.0.0.1:8000/api/create-user/${first_name}/${last_name}/`, {}, {timeout: 5000});
            getUser();
        } catch (error) {
            console.log('Erreur lors de l\'ajout des données: ', error);
        }
    }
    return (
        <form action="" className={style} onSubmit={handleSubmit}>
            <h1 className="text-xs mb-2 lg:self-start lg:text-sm">Entrez le nom et un prénom</h1>
            <Finput type = {"test"} placeholder={placeHolder} value={fullname} onChange={handleChange} style={styleInput}/>
            <Botton message={"+ Ajouter"}/>
        </form>
    )
}