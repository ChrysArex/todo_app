export default function Botton({message}) {
    const style = "w-24 h-10 bg-cyan-800 rounded-lg text-sm hover:bg-cyan-600"
    return (
        <button className = {style} type="submit">{message}</button>
    )
}