export default function Finput({type, onChange, value, placeholder, style}) {
   
    return (
        <input className = {style} type={type} onChange = {onChange} value={value} placeholder={placeholder} />
    )
}