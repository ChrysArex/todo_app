import React, { useState, useEffect } from 'react';
import axios from 'axios';

export default function HelloWorld({head, initials, message}) {
  
  //Logique de récupération du nom à faire
  return (
    <div className='flex flex-col justify-center items-start'>
      <div className='flex flex-row justify-start w-full mb-6 ml-4 lg:w-96'>
          <div className='flex flex-col justify-center items-center rounded-full bg-neutral-700 w-10 h-10 text-xl lg:text-2xl lg:w-14 lg:h-14'>
            {initials}
          </div>
          <h1 className='flex flex-col justify-center items-center text-base ml-5 lg:text-2xl'>{head}</h1>
      </div>
      <p className='text-justify text-xs w-3/4 pl-5 mb-10 lg:w-96'>
          Utilisez ce modèle pour suivre vos tâches personnelles.
          Cliquez sur <span className='bg-stone-700 rounded-sm px-2'>+ Ajouter</span> pour ajouter un nouvel Utilisateur ou une nouvelle tâche 
      </p>
      <div className='flex flex-row justify-around items-center w-36 ml-4 mb-3'>
        <i className="fa-solid fa-table"></i>
        <span className='text-sm'>Vue d'ensemble</span>
      </div>
      <div className='w-11/12 border-solid border-t bg-zinc-800 ml-5'>
      </div>
    </div>
  );
}