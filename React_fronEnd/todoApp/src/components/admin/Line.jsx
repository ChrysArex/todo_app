import Action from "./Action"
import { NavLink } from "react-router-dom"
import axios from "axios";
export default function Line({id,nom, prenom, getUser}) {
    const deleteUser = async () =>{
        axios.delete(`http://127.0.0.1:8000/api/delete-user/${id}`, {timeout: 5000}).then(res => {
            getUser()
        }).catch(error => {
            console.log('Erreur lors de la suppression des données : ', error);
        });
    }
    return (
        <tr className="h-12 hover:bg-zinc-800">
            <td>{id}</td>
            <td className="text-start">
                <NavLink to={'/' + nom + ':' + prenom + ':' + id}>
                    {nom + " " + prenom}
                </NavLink>
            </td>
            <td>
                <Action 
                    title={"Supprimer"} 
                    icon={<i className="fa-solid fa-trash"></i>}
                    Style = {"text-base hover:bg-red-600 px-2 py-1 rounded-lg mr-2 span cursor-pointer"}
                    onClick = {deleteUser}     
                />
            </td>
        </tr>
    )
}