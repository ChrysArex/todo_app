export default function Action({title, id, onClick, icon, Style}){
   
    return (
        <>
            <span className = {Style} title={title} onClick={onClick}>
                {icon}
            </span>
        </>
    )
}   