import { useState, useEffect } from 'react'
import Header from "./components/Header"
import Form from "./components/formulaire/Form"
import UserInterface from './components/user/UserInterface'
import Line from './components/admin/Line'
import axios from 'axios'
import {RouterProvider, NavLink,createBrowserRouter} from "react-router-dom"
import './css/App.css'

function App() {
  // les etats 
  const getUser = async () => {
    try {
      const response = await axios.get('http://127.0.0.1:8000/api/all-user/');
      setUsers(response.data);
    } catch (error) {
      if (error.code === 'ECONNABORTED') {
        console.log('La requête a expiré en raison d\'un délai d\'attente dépassé.');
      } else {
        console.log('Erreur lors de la requête:', error.message);
      }
    }
  };
  const [users, setUsers] = useState([])
  useEffect(() => {
    getUser();
  }, [users]);

  
  //les routes et render
  const router =  createBrowserRouter([
    {
      path: '/',
      element:
        <>
            <div className='container mx-auto'>
              <Header head = {"Tableau de bord"} initials = {"D"}/>
              <div className='flex flex-col justify-between mt-5 w-11/12  lg:flex-row '>
                <table className='table-fixed w-full ml-5  text-xs lg:w-8/12 lg:text-base'>
                  <thead className='h-10 bg-cyan-800 '>
                  <tr className=''>
                    <th>ID</th>
                    <th className="text-start">Utilisateur</th>
                    <th>Actions</th>
                  </tr>
                  </thead>
                  <tbody>
                    {
                      users.map((user) => {
                          return (
                            <Line 
                              key = {user.id} 
                              id = {user.id} 
                              nom = {user.first_name} 
                              prenom = {user.last_name} 
                              getUser={getUser}
                            />
                          )
                      })
                      
                    }
                  </tbody>
                  
                </table>
                <Form placeHolder = {"Nom complet"} getUser={getUser}/> 
              </div>
            </div>
        </>
    },
    {
      path: '/:slug',
      element: <UserInterface />
    }
  ])
  return <RouterProvider router={router} />
}

export default App
