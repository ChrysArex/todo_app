<h2>Ce dossier represente le Frontend de la todo app.</h2>

<h3>Installation:</h3>
<h5>Prérequis:</h5>

Vous auriez besoin de `node js` installé sur votre machine

**Déplacer vous vers le dossier React_fronEnd/todoApp et installer les dépendances avec la commande `npm install`**<br/>
<h3>Tester l'application:</h3>

**Une fois toutes les dépendances installées vous pouvez utiliser la commande  `npm run dev` pour lancer le server et faire les tests**
